<?php

namespace Drupal\counttoblock\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a block with a countto counter.
 *
 * @Block(
 *   id = "counttoblock",
 *   admin_label = @Translation("CountTo Block"),
 * )
 */
class CountToBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    return array(
      '#theme' => 'counttoblock',
      '#count_from' => $config['count_from'],
      '#count_to' => $config['count_to'],
      '#duration' => $config['duration'],
      '#interval' => isset($config['interval']) ? $config['interval'] : 100,
      '#subject' => isset($config['subject']) ? $config['subject'] : '',
      '#attached' => array(
        'library' => array(
          'counttoblock/counttoblock',
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['subject'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => isset($config['subject']) ? $config['subject'] : '',
      '#description' => $this->t('The subject, like "cups of coffee" or "happy customers"'),
      '#required' => TRUE,
    );

    $form['count_from'] = array(
      '#type' => 'number',
      '#title' => $this->t('Count from'),
      '#default_value' => isset($config['count_from']) ? $config['count_from'] : 0,
      '#required' => TRUE,
    );

    $form['count_to'] = array(
      '#type' => 'number',
      '#title' => $this->t('Count to'),
      '#default_value' => isset($config['count_to']) ? $config['count_to'] : 10,
      '#required' => TRUE,
    );

    $form['duration'] = array(
      '#type' => 'number',
      '#title' => $this->t('Duration'),
      '#description' => $this->t('The number of milliseconds it should take to finish counting.'),
      '#default_value' => isset($config['duration']) ? $config['duration'] : 1000,
      '#required' => TRUE,
    );

    $form['interval'] = array(
      '#type' => 'number',
      '#title' => $this->t('Refresh interval'),
      '#description' => $this->t('The number of milliseconds to wait between refreshing the counter.'),
      '#default_value' => isset($config['interval']) ? $config['interval'] : 100,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('subject', $form_state->getValue('subject'));
    $this->setConfigurationValue('count_from', $form_state->getValue('count_from'));
    $this->setConfigurationValue('count_to', $form_state->getValue('count_to'));
    $this->setConfigurationValue('duration', $form_state->getValue('duration'));
    $this->setConfigurationValue('interval', $form_state->getValue('interval'));
  }

}
