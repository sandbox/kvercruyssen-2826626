/**
 * @file
 * Block behaviors.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.counttoBlock = {
    attach: function (context, settings) {
      // Find each .block-counttoblock block, set up the basics and start counting.
      $(context).find('.block-counttoblock').once('counttoBlock').each(function () {
        var countNumber = $(this).find('.number');
        var countSubject = $(this).find('.subject');
        var countFrom = countNumber.data('countfrom') || 0;
        var countTo = countNumber.data('countto') || 100;
        var refreshInterval = countNumber.data('interval') || 100;
        var blockDuration = countNumber.data('duration') || 5000;

        $(countNumber).countTo({
          from: countFrom,
          to: countTo,
          speed: blockDuration,
          refreshInterval: refreshInterval,
          formatter: function (value, options) {
            return value.toFixed(options.decimals);
          },
          onUpdate: function (value) {},
          onComplete: function (value) {
            countSubject.delay(refreshInterval).animate({
              'opacity' : 1,
              'bottom' : '0px'
            }, 600, 'swing');
            $(countNumber).addClass('completed');
          }
        });
      });
    }
  };
})(jQuery, Drupal);
